package cdek.data.postgres;

import cdek.data.dao.EntityDAO;
import cdek.data.pojo.Entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*
* Реализация DAO для Postres
* */
public class EntityDAOImpl implements EntityDAO {

    private static final Logger LOG = LoggerFactory.getLogger(EntityDAOImpl.class);

    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplate;


    @Override
    public List<Entity> getAll() {
        String query = "SELECT * FROM entity ORDER by id";

        jdbcTemplate = new JdbcTemplate(dataSource);
        List<Entity> list = jdbcTemplate.query(query, new EntityMapper());

        return list;
    }

    @Override
    public List<Entity> getBySearch(String search) {
        String query = "SELECT * FROM entity WHERE name LIKE ?";

        jdbcTemplate = new JdbcTemplate(dataSource);
        List<Entity> list = new ArrayList<>();
        list = jdbcTemplate.query(query, new Object[] {"%"+search+"%"}, new EntityMapper());
//        List<Map<String, Object>> rows = jdbcTemplate.queryForList(query, new Object[] {search}, new EntityMapper());
//        for (Map row: rows) {
//            Entity entity = new Entity();
//            entity.setId((int) row.get("id"));
//            entity.setName((String) row.get("name"));
//            list.add(entity);
//        }

        return list;
    }

    @Override
    public void addElement(Entity element) {
        String query = "INSERT INTO entity(name) VALUES(?)";

        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(query, element.getName());
    }


    private static final class EntityMapper implements RowMapper<Entity> {
        @Override
        public Entity mapRow(ResultSet rs, int rowNum) throws SQLException {
            Entity element = new Entity();
            element.setId(rs.getInt("id"));
            element.setName(rs.getString("name"));
            return element;
        }
    }
}
