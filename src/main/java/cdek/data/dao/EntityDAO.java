package cdek.data.dao;

import cdek.data.pojo.Entity;
import java.util.List;


public interface EntityDAO {
    public List<Entity> getAll();
    public List<Entity> getBySearch(String search);
    public void addElement(Entity element);
//    public Entity getByID();
//    public void remove(int id);
}
