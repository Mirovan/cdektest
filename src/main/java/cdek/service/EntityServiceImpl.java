package cdek.service;

import cdek.data.dao.EntityDAO;
import cdek.data.pojo.Entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Сервис для работы с сущностью
 */
public class EntityServiceImpl implements EntityService {

    private static final Logger LOG = LoggerFactory.getLogger(EntityServiceImpl.class);

    @Autowired
    private EntityDAO entityDAO;

    @Override
    public List<Entity> getAll() {
        return entityDAO.getAll();
    }

    @Override
    public List<Entity> getBySearch(String search) {
        return entityDAO.getBySearch(search);
    }

    @Override
    public void addElement(Entity element) {
        entityDAO.addElement(element);
    }


}
