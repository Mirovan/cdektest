package cdek.service;

import cdek.data.pojo.Entity;

import java.util.List;

/**
 * Created by max on 2/28/2017.
 */
public interface EntityService {
    public List<Entity> getAll();
    public List<Entity> getBySearch(String search);
    public void addElement(Entity element);
}
