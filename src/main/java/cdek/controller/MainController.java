package cdek.controller;

import cdek.data.dao.EntityDAO;
import cdek.data.pojo.Entity;
import cdek.service.EntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {

    private static final Logger LOG = LoggerFactory.getLogger(MainController.class);

    @Autowired
    EntityService entityService;

    /**
     * Главная страницы - отображение всех записей
     * */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView showMainPage(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
        ModelAndView mav = new ModelAndView();

        String search = req.getParameter("search");

        try {
            List<Entity> data = null;
            if (search != null && !search.trim().equals("")) {
                //поиск по запросу
                data = entityService.getBySearch(search);
            } else {
                //выбрать все записи
                data = entityService.getAll();
            }

            mav.addObject("data", data);
            mav.setViewName("main");
        } catch (Exception e) {
            mav.setViewName("error");
        }

        return mav;
    }

    /**
     * Отображение страницы для добавления
     * */
    @RequestMapping(value = "/add/", method = RequestMethod.GET)
    public ModelAndView showAddingForm() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("add-form");
        return mav;
    }

    /**
     * Добавление в БД
     * */
    @RequestMapping(value = "/add/", method = RequestMethod.POST)
    public ModelAndView addElement(@ModelAttribute("record") Entity element) {
        ModelAndView mav = new ModelAndView();

        try {
            entityService.addElement(element);
            mav.setViewName("add-ok");
        } catch(Exception e) {
            mav.setViewName("error");
        }

        return mav;
    }

}
