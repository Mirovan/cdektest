<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="/WEB-INF/view/header.jsp"/>

<div class="blog-header">
    <h1 class="blog-title">Adding record</h1>
</div>


<div class="row">
    <form action="/add/" method="post">
        <div class="form-group">
            <label for="nameInput">Name</label>
            <input type="text" name="name" class="form-control" id="nameInput" value="">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" name="submit">
        </div>
    </form>
</div><!-- /.row -->

<c:import url="/WEB-INF/view/footer.jsp"/>