<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="/WEB-INF/view/header.jsp"/>

<div class="blog-header">
    <h1 class="blog-title">Records in table</h1>
</div>


<div class="row">
    <form action="/" method="GET">
        Search: <input type="text" name="search" />
        <input type="submit" />
    </form>
    <ol>
        <c:forEach var="rec" items="${data}">
            <li>
                ${rec.name} (id: ${rec.id})
            </li>
        </c:forEach>
    </ol>
</div><!-- /.row -->

<c:import url="/WEB-INF/view/footer.jsp"/>