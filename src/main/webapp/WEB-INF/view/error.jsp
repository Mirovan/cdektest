<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="/WEB-INF/view/header.jsp"/>

<div class="blog-header">
    <h1 class="blog-title">Error</h1>
</div>


<div class="row">
    <h2>Oooops, detect some error...</h2>
</div><!-- /.row -->

<c:import url="/WEB-INF/view/footer.jsp"/>